#!/usr/bin/python
import sys, getopt
import subprocess
import os
import binascii, struct

evaluation_time = '600'
GRID_ROW_SIZE = '10'
operation_str = 'none'

base_container_name1 = 'myubuntu'
ns3_path = "/home/ubuntu/workspace/source/ns-3.25/"

pids_directory = "./var/pid/"


def check_return_code(r_code, log):
    """
    calls sys.exit in case of error
    :param r_code: code return by subprocess call
    :param log: String to be shown in console
    """
    if r_code != 0:
        print "Error: %s" % log
        sys.exit(2)
    else:
        print "Success: %s" % log
    return


def check_return_code_passive(r_code, log):
    """
    verfies r_code and prints error string str in case of error
    :param r_code: code return by subprocess call
    :param log: String to be shown in console
    """
    if r_code != 0:
        print "Error: %s" % log
    else:
        print "Success: %s" % log
    return


def run_docker_containers(dir_path):
    """
    runs the numberOfNodes of containers
    :param dir_path:
    """
    try:
        subprocess.call(["docker-compose stop"], shell=True)  # stopping old containers
        out = subprocess.check_output(["docker-compose", "up", "-d"], stderr=subprocess.STDOUT)
        [containersNames.append(x.strip()) for x in out.split('Starting')]  # saving generated names of new containers
        containersNames.pop(0)  # will always be empty
    except subprocess.CalledProcessError, e:
        print "Exception in docker-compose up:\n", e.output


def create_bridge_and_tap_interfaces():
    """
    creates the bridges and the tap interfaces for NS3
    """
    acc_status = 0
    for x in containersNames:
        acc_status += subprocess.call("sudo bash scripts/create_bridge_and_tap_interfaces.sh %s" % myhash(x),
                                      shell=True)

    check_return_code(acc_status, "Creating bridge and tap interface")


def create_bridge_for_containers():
    acc_status = 0
    for x in range(0, len(containersNames)):
        cmd = ['docker', 'inspect', '--format', "'{{ .State.Pid }}'", containersNames[x]]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        pid = out[1:-2].strip()

        with open(pids_directory + myhash(containersNames[x]), "w") as text_file:
            text_file.write("%s" % (pid))

        acc_status += subprocess.call("sudo bash scripts/create_bridge_for_containers.sh %s %s %s"
                                      % (myhash(containersNames[x]), x, pid), shell=True)

    # If something went wrong creating the bridges and tap interfaces, we panic and exit
    check_return_code(acc_status, "Creating bridge side-int-X and side-ext-X")


def run_code_in_ns3():
    r_code = subprocess.call("cd ns3 && cp tap-wifi-virtual-machine.cc %s" % ns3_path + "/scratch/tap-vm.cc",
                             shell=True)
    if r_code != 0:
        print "Error copying latest ns3 file"
    else:
        print "NS3 up to date!"
        print "Go to NS3 folder, probably cd $NS3_HOME"
        print "Run sudo ./waf --run \"scratch/tap-vm --NumNodes=%s --TotalTime=%s --TapBaseName=emu\"" % (
            5, evaluation_time)
        print "or run sudo ./waf --run " \
              "\"scratch/tap-vm --NumNodes=%s --TotalTime=%s --TapBaseName=emu --SizeX=100 --SizeY=100\"" % (
                  5, evaluation_time)

    print "Done."


def create():
    print "Creating ..."

    if not os.path.exists(pids_directory):
        os.makedirs(pids_directory)

    build_docker_image()

    dir_path = os.path.dirname(os.path.realpath(__file__))
    run_docker_containers(dir_path)

    create_bridge_and_tap_interfaces()

    create_bridge_for_containers()

    # run_code_in_ns3()

    return


def ns3():
    print "NS3 ..."

    r_code = subprocess.call(
        "cd $NS3_HOME && sudo ./waf --run \"scratch/tap-vm --NumNodes=%s --TotalTime=%s --GridRowSize=%s --TapBaseName=emu\"" % (
            5, evaluation_time, GRID_ROW_SIZE), shell=True)
    if r_code != 0:
        print "NS3 WIN!"
    else:
        print "NS3 FAIL!"

    return


def build_docker_image():
    """makes sure that we are running the latest version of our Ubuntu container, as we need some tools available in
    latest version only.
    """

    r_code = subprocess.call("docker build -t %s ." % base_container_name1, shell=True)
    check_return_code(r_code, "Building regular container %s" % base_container_name1)

    r_code = subprocess.call("docker-compose build", shell=True)
    check_return_code(r_code, "docker-compose build")


def destroy():
    print "Destroying ..."

    r_code = subprocess.call("docker-compose stop", shell=True)
    check_return_code_passive(r_code, "docker-compose stop")

    for x in containersNames:

        r_code = subprocess.call("sudo bash scripts/destroy.sh %s" % myhash(x), shell=True)
        check_return_code_passive(r_code, "Destroying bridge and tap interface %s" % destroy)

        if os.path.exists(pids_directory + myhash(x)):
            with open(pids_directory + myhash(x), "rt") as in_file:
                text = in_file.read()
                r_code = subprocess.call("sudo rm -rf /var/run/netns/%s" % (text.strip()), shell=True)
                check_return_code_passive(r_code, "Destroying docker bridges %s" % myhash(x))
                subprocess.call("sudo rm -rf %s" % (pids_directory + myhash(x)), shell=True)

    return


# TODO use some efficient hashing algorithm
def myhash(s):
    return "XX%s" % containersNames.index(s)


# t == simulation time in seconds
###############################
# Cache an error with try..except
# Note: options is the string of option letters that the script wants to recognize, with
# options that require an argument followed by a colon (':') i.e. -i fileName
#
try:
    myopts, args = getopt.getopt(sys.argv[1:], "hn:o:t:p:", ["operation=", "time=", "path"])
except getopt.GetoptError as e:
    print (str(e))
    print("Usage: %s -o <create|destroy> -t emulationTime" % sys.argv[0])
    sys.exit(2)

for opt, arg in myopts:
    if opt == '-h':
        print("Usage: %s -o <create|destroy> -n numberOfNodes -t emulationTime -p ns3Home" % sys.argv[0])
        sys.exit()
    elif opt in ("-t", "--time"):
        evaluation_time = arg
    elif opt in ("-p", "--path"):
        ns3_path = arg
    elif opt in ("-o", "--operation"):
        operation_str = arg

# Display input and output file name passed as the args
print (
    "emulation time : %s and operation : %s" % (
        evaluation_time, operation_str))

emulationTime = int(evaluation_time)

containersNames = []
baseName = "emu"

if 'create' == operation_str:
    create()
elif 'destroy' == operation_str:
    destroy()
elif 'full' == operation_str:
    create()
    ns3()
    destroy()
else:
    print "Nothing to be done ..."
