Esper-Akka-JMS Integration
=========================

Using:
* Actors for event generation & processing 
* Esper for CEP
* ActiveMQ(JMS) for making everything distributed
* Docker for deployment
* Vagrant to host everything on Ubuntu 16.0.4

Problems faced so far:
* Documentation of simonstrator is incomplete
* Default Vagrant fails to compile NS3

Deploying scala app via "docker-compose"
* docker-compose build mainapp
* docker-compose up -d mainapp

Deploying this app via "docker-swarm":
* Create Nodes
    * docker-machine create --driver virtualbox manager 
    * docker-machine create --driver virtualbox worker1 
    * docker-machine create --driver virtualbox worker2

* Advertise & Join swarm
	* docker swarm init --advertise-addr <MANAGER-IP> # Run on manager and execute the command in output in worker nodes

* docker-machine scp docker-stack.yml manager:/home/docker/. #Copy stack file to manager
* docker stack deploy --compose-file docker-stack.yml  mainapp #Deploy stack

Using with NS3
==============
Use following command to setup enviroment:
```
python ./main.py -n 5 -t 100 -o create -p <path-of-ns3-source-folder>
```
This command will create bridges & tap interfaces

Run NS3 code with this command (in NS3 source folder):
```
sudo ./waf --run "scratch/tap-vm --NumNodes=5 --TotalTime=100 --TapBaseName=emu"
```
use this command to destroy containers, bridges & tap interfaces
```
python ./main.py -o destroy
```

Future Work
===========
* Remove git repo from Dockerfile
* Replace python + shell stack with something better e.g. Puppet, Chef or Ansible