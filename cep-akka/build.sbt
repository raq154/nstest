

name := """cep-akka"""

version := "1.0"

scalaVersion := "2.10.3"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.2.3",
  "org.apache.geronimo.specs" % "geronimo-jms_1.1_spec" % "1.1.1",
  "com.typesafe" % "config" % "1.3.1",
  "redis.clients" % "jedis" % "2.9.0",
  "org.apache.activemq" % "activemq-broker" % "5.14.4",
  "org.apache.activemq" % "activemq-client" % "5.14.4",
  "org.apache.activemq" % "activemq-core" % "5.7.0",
  "com.espertech" % "esper" % "4.11.0")

libraryDependencies ~= {
  _.map(_.exclude("org.slf4j", "slf4j-log4j12"))
}

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.0.9"


import com.github.retronym.SbtOneJar._

oneJarSettings
libraryDependencies += "commons-lang" % "commons-lang" % "2.6"

mainClass in(Compile, run) := Some("de.kom.tud.cep.Main")
mainClass in oneJar := Some("de.kom.tud.cep.Main")
