package de.kom.tud.cep

import java.net.{DatagramSocket, InetAddress, _}
import java.nio.ByteBuffer
import java.nio.channels.DatagramChannel
import java.util
import java.util.concurrent.CountDownLatch

import org.slf4j.LoggerFactory

/**
  * Created by raheel
  * on 4/8/17.
  */
object UDPBeacon extends App {
  def logger = LoggerFactory.getLogger(this.getClass)

  logger.debug("Starting System ...")
  val latch: CountDownLatch = new CountDownLatch(1000)
  val beacon: Array[Byte] = "beacon".getBytes
  val prefix: Array[Byte] = "prefix:".getBytes
  val port: Int = 8888

  Thread.sleep(2000)

  val zbeacon: ZBeacon = new ZBeacon("255.255.255.255", port, beacon, false)
  zbeacon.setPrefix(prefix)
  zbeacon.setListener(new Listener {
    override def onBeacon(sender: InetSocketAddress, beacon: String): Unit = {
      logger.info("received message from " + sender.getAddress)
      latch.countDown()
    }
  })

  zbeacon.start()
  latch.await()
}


trait Listener {
  def onBeacon(sender: InetSocketAddress, beacon: String)
}

class ZBeacon(val host: String, val port: Int, val beacon: Array[Byte], val ignoreLocalAddress: Boolean) {
  val DEFAULT_BROADCAST_INTERVAL: Long = 3000L

  val broadcastInetAddress: InetAddress = InetAddress.getByName(host)
  val broadcastClient: ZBeacon#BroadcastClient = new BroadcastClient
  val broadcastServer: ZBeacon#BroadcastServer = new BroadcastServer(ignoreLocalAddress)

  var listener: Listener = _
  var prefix: Array[Byte] = Array()

  broadcastServer.setDaemon(true)
  broadcastClient.setDaemon(true)


  def this(host: String, port: Int, beacon: Array[Byte]) {
    this(host, port, beacon, true)
  }

  def setUncaughtExceptionHandlers(clientHandler: Thread.UncaughtExceptionHandler, serverHandler: Thread.UncaughtExceptionHandler) {
    broadcastClient.setUncaughtExceptionHandler(clientHandler)
    broadcastServer.setUncaughtExceptionHandler(serverHandler)
  }

  def start() {
    if (listener != null) broadcastServer.start()
    broadcastClient.start()
  }

  @throws[InterruptedException]
  def stop() {
    if (broadcastClient != null) {
      broadcastClient.interrupt()
      broadcastClient.join()
    }
    if (broadcastServer != null) {
      broadcastServer.interrupt()
      broadcastServer.join()
    }
  }

  def setPrefix(prefix: Array[Byte]) {
    this.prefix = prefix
  }

  def getPrefix: Array[Byte] = prefix

  def setListener(listener: Listener) {
    this.listener = listener
  }

  def getListener: Listener = listener


  /**
    * The broadcast client periodically sends beacons via UDP to the network.
    */
  class BroadcastClient() extends Thread {
    private val broadcastInetSocketAddress = new InetSocketAddress(broadcastInetAddress, port)
    private val broadcastChannel: DatagramChannel = DatagramChannel.open
    val logger = LoggerFactory.getLogger(this.getClass)
    broadcastChannel.socket.setBroadcast(true)

    override def run() {
      logger.info("broadcasting message")

      while (!isInterrupted) {
        try {
          broadcastChannel.send(ByteBuffer.wrap(beacon), broadcastInetSocketAddress)
          Thread.sleep(DEFAULT_BROADCAST_INTERVAL)
        } catch {
          case ex: Exception => logger.error(ex.getMessage, ex)
        }
      }
      broadcastChannel.close()
    }
  }

  /**
    * The broadcast server receives beacons.
    */
  class BroadcastServer(val ignoreLocalAddress: Boolean) extends Thread {
    private var handle: DatagramChannel = _

    def logger = LoggerFactory.getLogger(this.getClass)

    // Create UDP socket
    handle = DatagramChannel.open
    handle.configureBlocking(false)
    val sock: DatagramSocket = handle.socket
    sock.setReuseAddress(true)
    sock.bind(new InetSocketAddress("0.0.0.0", port))


    override def run() {
      val buffer: ByteBuffer = ByteBuffer.allocate(65535)
      var sender: SocketAddress = null
      var size: Int = 0

      while (!isInterrupted) {
        buffer.clear
        val read: Int = buffer.remaining
        sender = handle.receive(buffer)
        if (sender != null) {


          val senderAddress: InetAddress = sender.asInstanceOf[InetSocketAddress].getAddress
          val messageContent = new String(buffer.array(), "ASCII")

          if (messageContent.trim == "beacon") {
            listener.onBeacon(sender.asInstanceOf[InetSocketAddress], messageContent)
          }
        }
      }
      handle.socket.close()
    }
  }

}
