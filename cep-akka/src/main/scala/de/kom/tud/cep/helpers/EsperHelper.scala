package de.kom.tud.cep.helpers

import com.espertech.esper.client.{Configuration, EPServiceProvider, EPServiceProviderManager}
import de.kom.tud.cep.models.Stock

/**
  * Created by mac on 3/27/17.
  */
object EsperHelper {

  private val configuration: Configuration = new Configuration
  configuration.addEventType("Stock", classOf[Stock])
  private lazy val engine: EPServiceProvider = EPServiceProviderManager.getDefaultProvider(configuration)

  def getEngine = engine
}
