package de.kom.tud.cep.utils

import com.typesafe.config.{Config, ConfigFactory}

/**
  * Created by raheel
  * on 3/27/17.
  */
object ConfigurationUtils {

  //@formatter:off
  private def isDevMode: Boolean = System.getProperty("env") == null || System.getProperty("env") == "development"
  def environment: String = if (System.getProperty("env") == null) "development" else System.getProperty("env")

  private def getConfiguration: Config = ConfigFactory.load("myapp.conf").getConfig(environment)

  def host: String = getConfiguration.getString(Configuration.URL)
  def userName: String = getConfiguration.getString(Configuration.USER_NAME)
  def password: String = getConfiguration.getString(Configuration.PASSWORD)
  def jmsContextFactory = getConfiguration.getString(Configuration.JMS_CONTEXT_FACTORY)
  def jmsConnectionFactory = getConfiguration.getString(Configuration.JMS_CONNECTON_FACTORY)
  def jmsIncomingDestination = getConfiguration.getString(Configuration.JMS_INCOMING_DESTINATION)
  def jmsIsTopic = getConfiguration.getBoolean(Configuration.JMS_IS_TOPIC)

  def redisHost = getConfiguration.getString(Configuration.REDIS_HOST)
  def redisAvgCostKey = getConfiguration.getString(Configuration.REDIS_AVG_COST_KEY)

  private object Configuration {
    val URL                       = "jms-url"
    val USER_NAME                 = "jms-user-name"
    val PASSWORD                  = "jms-password"
    val JMS_CONTEXT_FACTORY       = "jms-context-factory"
    val JMS_CONNECTON_FACTORY     = "jms-connection-factory-name"
    val JMS_INCOMING_DESTINATION  = "jms-incoming-destination"
    val JMS_IS_TOPIC              = "jms-is-topic"

    val REDIS_HOST                = "redis-host"
    val REDIS_AVG_COST_KEY        = "redis-price-key"
  }
}