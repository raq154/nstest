package de.kom.tud.cep.actors

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, Cancellable}
import de.kom.tud.cep.helpers.JMSHelper
import de.kom.tud.cep.models.Stock
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.FiniteDuration

/**
  * Created by raheel
  * on 3/23/17.
  */
class StocksGenerator extends Actor {
  def logger = LoggerFactory.getLogger(this.getClass)

  val Tick = "tick"
  var timer: Cancellable = _
  val randomStockPrice = System.currentTimeMillis() % 100

  override def preStart(): Unit = {
    super.preStart()

    timer =
      context.system.scheduler.schedule(
        FiniteDuration(15000, TimeUnit.MILLISECONDS),
        FiniteDuration(15000, TimeUnit.MILLISECONDS),
        self,
        Tick)
  }

  override def receive: Receive = {
    case Tick => JMSHelper.getJMSContext.sendMessage(new Stock("Apple", randomStockPrice))
  }

  override def postStop(): Unit = {
    super.postStop()
    timer.cancel()
  }
}
