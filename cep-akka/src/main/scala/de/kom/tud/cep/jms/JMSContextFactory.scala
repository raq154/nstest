package de.kom.tud.cep.jms

import java.util
import javax.jms._
import javax.naming.{Context, InitialContext, NamingException}

/**
  * Created by raheel
  * on 3/28/17.
  */
object JMSContextFactory {
  @throws[JMSException]
  @throws[NamingException]
  def createContext(ctxFactory: String, url: String, connectionFactoryName: String, user: String, pwd: String,
                    jmsDestination: String, isTopic: Boolean): JMSContext = {
    val factory: ConnectionFactory = getConnectionFactory(ctxFactory, url, connectionFactoryName, user, pwd)
    val connection: Connection = factory.createConnection(user, pwd)
    val session: Session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE)
    var destination: Destination = null
    if (isTopic) destination = session.createTopic(jmsDestination)
    else destination = session.createQueue(jmsDestination)

    connection.start()


    val producer = session.createProducer(destination)

    new JMSContext(factory, connection, session, destination, producer)
  }

  @SuppressWarnings(Array("unchecked"))
  @throws[NamingException]
  private def getConnectionFactory(ctxFactory: String, url: String, connectionFactoryName: String,
                                   user: String, pwd: String): ConnectionFactory = {
    val env = new util.Hashtable[String, String]()
    env.put(Context.INITIAL_CONTEXT_FACTORY, ctxFactory)
    env.put(Context.PROVIDER_URL, url)
    env.put(Context.SECURITY_PRINCIPAL, user)
    env.put(Context.SECURITY_CREDENTIALS, pwd)
    val jndiContext: InitialContext = new InitialContext(env)
    jndiContext.lookup(connectionFactoryName).asInstanceOf[ConnectionFactory]
  }
}
