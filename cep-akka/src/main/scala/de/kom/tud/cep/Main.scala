package de.kom.tud.cep

import akka.actor.{ActorSystem, Props}
import de.kom.tud.cep.actors.{StocksGenerator, StocksProcessor}
import de.kom.tud.cep.helpers.JMSHelper
import de.kom.tud.cep.utils.ConfigurationUtils
import org.apache.log4j.BasicConfigurator
import org.slf4j.LoggerFactory

/**
  * Created by raheel
  * on 3/23/17.
  */
object Main extends App {
  def logger = LoggerFactory.getLogger(this.getClass)

  System.setProperty("org.apache.activemq.SERIALIZABLE_PACKAGES", "*")
  JMSHelper.startListening()

  val system = ActorSystem()
  BasicConfigurator.configure()

  system.actorOf(Props(new StocksProcessor()))

  for (i <- 1 to 10) {
    system.actorOf(Props(new StocksGenerator()))
  }

  logger.info(s"booting in ${ConfigurationUtils.environment} \n")
}
