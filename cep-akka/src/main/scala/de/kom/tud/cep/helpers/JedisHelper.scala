package de.kom.tud.cep.helpers

import java.net.URI

import de.kom.tud.cep.utils.ConfigurationUtils
import redis.clients.jedis.{Jedis, JedisPool}

/**
  * Created by raheel
  * on 3/30/17.
  */
object JedisHelper {
  private lazy val pool: JedisPool = new JedisPool(new URI(ConfigurationUtils.redisHost), 100)

  def getJedisInstance: Jedis = pool.getResource

  def freeResource(jedis: Jedis) {
    jedis.close()
  }
}
