package de.kom.tud.cep.actors

import akka.actor.Actor
import com.espertech.esper.client._
import de.kom.tud.cep.helpers.{EsperHelper, JedisHelper}
import de.kom.tud.cep.utils.ConfigurationUtils
import org.slf4j.LoggerFactory

/**
  * Created by raheel
  * on 3/23/17.
  */

class StocksProcessor extends Actor with UpdateListener {

  def logger = LoggerFactory.getLogger(this.getClass)

  private val jedis = JedisHelper.getJedisInstance

  override def preStart(): Unit = {
    super.preStart()
    val expression: String = "select avg(price) from Stock.win:time(60 sec)"
    val statement: EPStatement = EsperHelper.getEngine.getEPAdministrator.createEPL(expression)
    statement.addListener(this)
  }

  override def update(newEvents: Array[EventBean], oldEvents: Array[EventBean]): Unit = {
    val event: EventBean = newEvents(0)
    logger.info("avg = " + event.get("avg(price)"))
    jedis.set(ConfigurationUtils.redisAvgCostKey, event.get("avg(price)").toString)
  }

  override def receive: Receive = {
    case x: Any => unhandled(x)
  }

  override def postStop(): Unit = {
    super.postStop()
    JedisHelper.freeResource(jedis)
  }
}
