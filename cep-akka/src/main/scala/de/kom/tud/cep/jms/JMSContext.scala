package de.kom.tud.cep.jms

import javax.jms._

import de.kom.tud.cep.models.Stock
import org.slf4j.LoggerFactory

/**
  * Created by raheel
  * on 3/28/17.
  */
class JMSContext(val factory: ConnectionFactory, val connection: Connection, val session: Session,
                 val destination: Destination, val producer: MessageProducer) {
  def logger = LoggerFactory.getLogger(this.getClass)

  def destroy() {
    session.close()
    connection.close()
  }

  def getConnection: Connection = connection

  def getDestination: Destination = destination

  def getFactory: ConnectionFactory = factory

  def getSession: Session = session

  def sendMessage(message: Stock): Unit = {
    logger.info("sending message")
    val msg: ObjectMessage = session.createObjectMessage(message)
    producer.send(msg)
  }
}
