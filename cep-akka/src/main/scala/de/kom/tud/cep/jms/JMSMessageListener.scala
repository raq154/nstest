package de.kom.tud.cep.jms

import javax.jms.{Message, MessageListener, ObjectMessage}

import com.espertech.esper.client.EPRuntime
import de.kom.tud.cep.models.Stock
import org.slf4j.LoggerFactory

/**
  * Created by raheel
  * on 3/28/17.
  */

class JMSMessageListener(var engine: EPRuntime) extends MessageListener {
  def logger = LoggerFactory.getLogger(this.getClass)

  def onMessage(message: Message) {
    message match {

      case objectMessage: ObjectMessage => {
        objectMessage.getObject match {
          case stock: Stock => engine.sendEvent(stock)
          case _ => logger.error("unexpected message found")
        }
      }

      case _ => logger.error("Only expecting ObjectMessages")
    }
  }
}