package de.kom.tud.cep.models

/**
  * Created by Raheel
  * on 3/23/17.
  */
class Stock(itemName: String, price: Double) extends Serializable {
  def getItemName: String = itemName

  def getPrice: Double = price

  override def toString: String = {
    s"{itemName:$itemName, price:$price}"
  }
}
