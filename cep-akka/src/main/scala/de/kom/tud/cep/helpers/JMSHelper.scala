package de.kom.tud.cep.helpers

import javax.jms._

import de.kom.tud.cep.jms.{JMSContext, JMSContextFactory, JMSMessageListener}
import de.kom.tud.cep.utils.ConfigurationUtils._

/**
  * Created by raheel
  * on 3/27/17.
  */
object JMSHelper {
  def getJMSContext: JMSContext = jmsCtx

  lazy val jmsCtx: JMSContext = JMSContextFactory.createContext(jmsContextFactory, host,
    jmsConnectionFactory, userName, password,
    jmsIncomingDestination, jmsIsTopic)

  lazy val listener: JMSMessageListener = new JMSMessageListener(EsperHelper.getEngine.getEPRuntime)
  lazy val consumer: MessageConsumer = jmsCtx.getSession.createConsumer(jmsCtx.getDestination)

  def startListening() = consumer.setMessageListener(listener)
}